This target is only valid in the
.B nat
table, in the
.B PREROUTING
and
.B OUTPUT
chains, and user-defined chains which are only called from those
chains.  It specifies that the destination address of the packet
should be modified (and all future packets in this connection will
also be mangled), and rules should cease being examined.  It takes the
following options:
.TP
\fB\-\-to\-destination\fP [\fIipaddr\fP[\fB\-\fP\fIipaddr\fP]][\fB:\fP\fIport\fP[\fB\-\fP\fIport\fP]]
which can specify a single new destination IP address, an inclusive
range of IP addresses. Optionally a port range,
if the rule also specifies one of the following protocols:
\fBtcp\fP, \fBudp\fP, \fBdccp\fP or \fBsctp\fP.
If no port range is specified, then the destination port will never be
modified. If no IP address is specified then only the destination port
will be modified.
.TP
\fB\-\-random\fP
If option
\fB\-\-random\fP
is used then port mapping will be randomized (kernel >= 2.6.22).
.TP
\fB\-\-persistent\fP
Gives a client the same source-/destination-address for each connection.
This supersedes the SAME target. Support for persistent mappings is available
from 2.6.29-rc2.
.TP
IPv6 support available since Linux kernels >= 3.7.
